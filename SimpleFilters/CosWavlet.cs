﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFilters
{
    public class CosWavlet : SimpleFilters.Sinusodial
    {

        public CosWavlet(int dimension)
            : base(dimension)
        {


        }

        public override double wavletValue(int phi, int dimension)
        {
            double omega = (phi / (double)dimension / 2);
            //return Sine(wPI)
            return Math.Cos(omega * Math.PI);
        }


        public double wavletValue(int phi)
        {
            double omega = (phi / (double)this.dimension / 2);
            //return Sine(wPI)
            return Math.Cos(omega * Math.PI);

        }

    }
}
