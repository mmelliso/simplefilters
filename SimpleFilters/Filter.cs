﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFilters
{

    interface Filter
    {

        double getFilter(int row, int column);
        void setFilter(int row, int column, double value);
        void setFilter(double[,] filterValue);
        void multiplyWithFilter(ParentFilter multipleFilter);
        void generateFilter();
        double wavletValue(int int1, int int2);
        int getDimension();
        
    }

    public abstract class ParentFilter : IDisposable
    {


        protected int dimension;
        protected double[,] filterArray;

        protected ParentFilter(int dimension)
        {

            this.dimension = dimension;
            this.filterArray = new double[dimension, dimension];

        }

        public double getFilter(int row, int column)
        {
            return this.filterArray[row, column];
        }

        public void setFilter(int row, int column, double value)
        {
            this.filterArray[row, column] = value;
        }

        public void setFilter(double[,] filterValue)
        {
            this.filterArray = filterValue;
        }

        public void multiplyWithFilter(ParentFilter multipleFilter)
        {

            if (this.dimension != multipleFilter.dimension)
            {

                throw new ArgumentException("The given filter must have the same dimension as this filter.\nThis filter's dimenson is: " + this.dimension);

            }

            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {

                    this.filterArray[i, j] *= multipleFilter.getFilter(i, j);

                }

            }


        }

        public int getDimension()
        {
            return this.dimension;
        }

        public void Dispose()
        {

            this.filterArray = null;

        }

    }
}
