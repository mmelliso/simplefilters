﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFilters
{

    public class Gaussian : SimpleFilters.ParentFilter, SimpleFilters.Filter, IDisposable
    {

        protected double peak;
        protected double alpha;
        protected double beta;

        public Gaussian(int dimension, double peak) 
            : base(dimension)
        {

            this.peak = peak;
            this.alpha = dimension * 0.4770322291;
            this.beta = this.alpha;

            this.generateFilter();

        }

        public Gaussian(int dimension)
            : base(dimension)
        {
            this.peak = 15;
            this.alpha = dimension * 0.4770322291;
            this.beta = this.alpha;

            this.generateFilter();

        }

        public double wavletValue(int rho, int phi)
        {

            return this.peak * Math.Exp(-Math.Pow((double)rho, 2) / Math.Pow((double)alpha, 2)) * Math.Exp(-Math.Pow((double)phi, 2) / Math.Pow((double)beta, 2));

        }

        public void generateFilter()
        {

            int rho;
            int phi;

            for (int i = 0; i < this.dimension; i++)
            {

                rho = i - (this.dimension / 2);

                for (int j = 0; j < this.dimension; j++)
                {

                    phi = j - (this.dimension / 2);
                    filterArray[i, j] = wavletValue(rho, phi);

                }

            }

        }


    }
}
