﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFilters
{
    public abstract class Sinusodial : SimpleFilters.ParentFilter, SimpleFilters.Filter
    {

        public Sinusodial(int dimension)
            : base(dimension)
        {
            this.generateFilter();
        }

        public void generateFilter()
        {

            
            int phi;
            double sum = 0;
            double rowSum = 0;

            for (int j = 0; j < this.dimension; j++)
            {

                phi = j - (this.dimension / 2);
                base.filterArray[0, j] = wavletValue(phi, dimension);
                sum += base.filterArray[0, j];

            }

            for (int j = 0; j < this.dimension; j++)
            {

                base.filterArray[0, j] -= (sum / this.dimension);

            }

            for (int i = 0; i < this.dimension; i++)
            {

                for (int j = 0; j < this.dimension; j++)
                {

                    base.filterArray[i, j] = filterArray[0, j];

                }

            }

            SimpleFilters.Gaussian Gaussian = new Gaussian(this.dimension);
            this.multiplyWithFilter(Gaussian);
            Gaussian.Dispose();

            for (int i = 0; i < this.dimension; i++)
            {

                rowSum = 0;

                for (int j = 0; j < this.dimension; j++)
                {

                    rowSum += base.filterArray[i, j];

                }

                for (int j = 0; j < this.dimension; j++)
                {

                    base.filterArray[i, j] -= rowSum / (double) this.dimension;


                }

            }

        }

        public virtual double wavletValue(int phi, int dimension)
        {
            Console.WriteLine("Wrong Value Function!");
            return 0.0;
        }


    }
}
